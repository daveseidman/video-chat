import './index.scss';
import onChange from 'on-change';
import RTCMultiConnection from './assets/RTCMultiConnection';
import Kuula from './modules/Kuula';
import Menu from './modules/Menu';
import Overlays from './modules/Overlays';
import Admin from './modules/Admin';
import Chat from './modules/Chat';
import Connect from './modules/Connect';

const randomCode = () => [...Array(4)].map(() => String.fromCharCode(65 + Math.floor(Math.random() * 26))).join('');

class App {
  constructor() {
    this.joinRoom = this.joinRoom.bind(this);
    this.update = this.update.bind(this);

    const state = {
      connection: null,
      host: false,
      roomID: null,
      clients: [],
      chat: false,
      overlay: null,
      connect: false,
      survey: false,
      docs: false,
    };
    this.state = onChange(state, this.update);

    this.state.connection = new RTCMultiConnection();
    this.state.connection.socketURL = '/';
    if (window.location.href.indexOf('localhost') === -1) {
      this.state.connection.socketOptions = { path: '/booth/socket.io/' };
    }

    this.state.connection.session = { audio: true, video: true, data: true };

    this.state.connection.sdpConstraints.mandatory = {
      OfferToReceiveAudio: true,
      OfferToReceiveVideo: true,
    };

    this.state.connection.mediaConstraints = { video: { width: { ideal: 1280 }, height: { ideal: 720 }, frameRate: 30 }, audio: true };

    this.state.connection.processSdp = (sdp) => {
      const codecs = 'vp8';
      sdp = this.state.connection.CodecsHandler.preferCodec(sdp, codecs.toLowerCase());
      sdp = this.state.connection.CodecsHandler.setApplicationSpecificBandwidth(sdp, { audio: 128, video: 512, screen: 512 });
      sdp = this.state.connection.CodecsHandler.setVideoBitrates(sdp, { min: 512 * 8 * 1024, max: 512 * 8 * 1024 });

      return sdp;
    };


    this.element = document.createElement('div');
    this.element.className = 'app';

    this.menu = new Menu(this.state);
    this.kuula = new Kuula(this.state);
    this.chat = new Chat(this.state);
    this.overlays = new Overlays(this.state);
    this.connect = new Connect(this.state);

    this.element.appendChild(this.kuula.element);
    this.element.appendChild(this.chat.element);
    this.element.appendChild(this.connect.element);
    this.element.appendChild(this.overlays.element);
    this.element.appendChild(this.menu.element);


    this.state.connection.onstream = (e) => { this.state.clients.push(e); };

    const urlSearchParams = new URLSearchParams(window.location.search);
    if (urlSearchParams.has('admin')) {
      this.admin = new Admin(this.state);
      this.menu.removeConnectIcon();
      this.element.appendChild(this.admin.element);
    }
    if (urlSearchParams.has('guest')) {
      this.connect.name.value = urlSearchParams.get('guest');
    }
    if (urlSearchParams.has('roomid')) {
      this.connect.code.value = urlSearchParams.get('roomid');
      this.connect.checkCode();
      this.state.connect = true;
    }
  }


  update(path, current, prev) {
    console.log('state changed:', path, ':', typeof (prev) !== 'function' ? prev : '', '->', typeof (current) !== 'function' ? current : '');

    if (path === 'connect') {
      this.connect[current ? 'show' : 'hide']();
    }

    if (path === 'host') {
      if (current) {
        this.createRoom();
      }
    }

    if (path === 'roomID' && !prev && current) {
      this.joinRoom(current);
      this.connect.disable();
    }

    if (path === 'clients') {
      // TODO: maybe redundant to enable and show at once?
      if (current.length >= 2) {
        this.state.chat = true;
        this.chat.enable();
      }
      this.chat.videoChat.addStream(current[current.length - 1]);
      this.connect.hide();
    }

    if (path === 'overlay') {
      if (current) this.overlays.show(current);
      else this.overlays.hide();
    }

    if (path === 'chat') {
      this.chat[current ? 'show' : 'hide']();
    }
  }

  createRoom() {
    this.state.connection.open(randomCode(), (isRoomOpened, roomid, error) => {
      if (!error) {
        this.admin.showCode(roomid);
      }
    });
  }

  joinRoom(roomID) {
    if (this.connect.name.value !== '') {
      console.log(this.state.connection);
      this.state.connection.userid = this.connect.name.value;
      console.log(this.state.connection);
    }
    this.state.connection.join(roomID, (isJoinedRoom, roomid, error) => {
      if (error === 'Room not available') {
        this.connect.enable();
        this.connect.code.classList.add('error');
        this.connect.title.innerText = 'room not found';
      }

      if (!error) {
        console.log('successfully connected to host');
      }
    });
  }
}

const app = new App();
document.body.appendChild(app.element);
window.app = app;
