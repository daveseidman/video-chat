export default class Kuula {
  constructor() {
    this.element = document.createElement('iframe');
    this.element.className = 'kuula';
    this.element.setAttribute('src', 'https://kuula.co/share/collection/7lbwt?fs=1&vr=1&thumbs=-1&chromeless=1&logo=-1');
    this.element.setAttribute('width', '100%');
    this.element.setAttribute('height', '100%');
    this.element.setAttribute('frameborder', 0);
    this.element.setAttribute('allowfullscreen', 'true');
    this.element.setAttribute('allow', 'vr,gyroscope,accelerometer,fullscreen');
    this.element.setAttribute('scrolling', 'no');
  }

  update() {
    // TODO: look into API, see if it's possible to let the host drive visitors tours
  }
}
