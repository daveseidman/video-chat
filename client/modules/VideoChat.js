export default class VideoChat {
  constructor(state) {
    this.state = state;
    this.addStream = this.addStream.bind(this);
    this.element = document.createElement('div');
    this.element.className = 'videochat';

    this.state.connection.onstreamended = this.streamEnded;
    this.state.connection.onMediaError = this.mediaError;
  }

  addStream(event) {
    event.mediaElement.removeAttribute('src');
    event.mediaElement.removeAttribute('srcObject');
    event.mediaElement.muted = true;
    event.mediaElement.volume = 0;

    const user = document.createElement('div');
    const video = document.createElement('video');
    const label = document.createElement('div');

    label.innerText = event.type === 'local' ? 'you' : event.userid;

    user.className = `videochat-user ${event.type}`;
    video.className = 'videochat-user-video';
    label.className = 'videochat-user-label';

    try {
      video.setAttributeNode(document.createAttribute('autoplay'));
      video.setAttributeNode(document.createAttribute('playsinline'));
    } catch (e) {
      video.setAttribute('autoplay', true);
      video.setAttribute('playsinline', true);
    }

    if (event.type === 'local') {
      video.volume = 0;
      try {
        video.setAttributeNode(document.createAttribute('muted'));
      } catch (e) {
        video.setAttribute('muted', true);
      }
    }
    video.srcObject = event.stream;
    video.id = event.streamid;

    user.appendChild(video);
    user.appendChild(label);
    this.element.appendChild(user);

    setTimeout(() => {
      video.play(); // TODO: maybe this is here in case some platforms freeze?
    }, 5000);


    // to keep room-id in cache
    // localStorage.setItem(this.state.connection.socketMessageEvent, this.state.connection.sessionid);

    // chkRecordConference.parentNode.style.display = 'none';

    // if (event.type === 'local') {
    //   this.state.connection.socket.on('disconnect', () => {
    //     if (!this.state.connection.getAllParticipants().length) {
    //       window.location.reload();
    //     }
    //   });
    // }
  }

  streamEnded(event) {
    console.log('remove video here', event.streamid);
    // this.videoContainer.removeChild(document.getElementById(event.streamid));
    // const mediaElement = document.getElementById(event.streamid);
    // if (mediaElement) {
    //   mediaElement.parentNode.removeChild(mediaElement);
    // }
  }

  mediaError(e) {
    console.log('here', e);
    if (e.message === 'Concurrent mic process limit.') {
      if (DetectRTC.audioInputDevices.length <= 1) {
        alert('Please select external microphone. Check github issue number 483.');
        return;
      }

      const secondaryMic = DetectRTC.audioInputDevices[1].deviceId;
      this.state.connection.mediaConstraints.audio = {
        deviceId: secondaryMic,
      };

      this.state.connection.join(this.state.connection.sessionid);
    }
  }
}
