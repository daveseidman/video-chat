export default class Resources {
  constructor() {
    this.element = document.createElement('div');
    this.element.className = 'resources';

    this.title = document.createElement('h1');
    this.title.innerText = 'Resources';
    this.title.className = 'resources-title';

    this.content = document.createElement('div');
    this.content.className = 'resources-content';

    this.downloads = document.createElement('div');
    this.downloads.className = 'resources-content-downloads';
    this.downloadPDF = document.createElement('button');
    this.downloadPDF.innerText = 'PDF Download';

    this.downloadISI = document.createElement('button');
    this.downloadISI.innerText = 'ISI Download';

    this.downloads.appendChild(this.downloadPDF);
    this.downloads.appendChild(this.downloadISI);

    this.signup = document.createElement('div');
    this.signup.className = 'resources-content-signup';

    this.signupLabel = document.createElement('p');
    this.signupLabel.className = 'resources-content-signup-label';
    this.signupLabel.innerText = 'sign up for updates';

    this.signupEmail = document.createElement('input');
    this.signupEmail.className = 'resources-content-signup-email';
    this.signupEmail.type = 'text';
    this.signupEmail.placeholder = 'email address';

    this.signupButton = document.createElement('button');
    this.signupButton.className = 'resources-content-signup-button';
    this.signupButton.innerText = '>';

    this.signup.appendChild(this.signupLabel);
    this.signup.appendChild(this.signupEmail);
    this.signup.appendChild(this.signupButton);

    this.content.appendChild(this.downloads);
    this.content.appendChild(this.signup);

    this.element.appendChild(this.title);
    this.element.appendChild(this.content);
  }
}
