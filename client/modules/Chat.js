import VideoChat from './VideoChat';
import TextChat from './TextChat';

export default class Chat {
  constructor(state) {
    this.state = state;

    this.enable = this.enable.bind(this);
    this.disable = this.disable.bind(this);

    this.element = document.createElement('div');
    this.element.className = 'chat hidden disabled';

    this.videoChat = new VideoChat(state);
    this.textChat = new TextChat(state);

    this.close = document.createElement('button');
    this.close.className = 'chat-close';
    this.close.innerText = '×';
    this.close.addEventListener('click', () => { this.state.chat = false; });

    this.element.appendChild(this.videoChat.element);
    this.element.appendChild(this.textChat.element);
  }

  show() {
    this.element.classList.remove('hidden');
  }

  hide() {
    this.element.classList.add('hidden');
  }

  enable() {
    this.element.classList.remove('disabled');
  }

  disable() {
    this.element.classList.add('disabled');
  }
}
