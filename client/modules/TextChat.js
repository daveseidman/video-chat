export default class TextChat {
  constructor(state) {
    this.state = state;

    this.addMessage = this.addMessage.bind(this);
    this.sendMessage = this.sendMessage.bind(this);

    this.element = document.createElement('div');
    this.element.className = 'textchat';

    this.messages = document.createElement('div');
    this.messages.className = 'textchat-messages';

    this.compose = document.createElement('div');
    this.compose.className = 'textchat-compose';

    this.message = document.createElement('input');
    this.message.className = 'textchat-compose-message';
    this.message.type = 'text';
    this.message.placeholder = 'Chat Message Here';

    this.send = document.createElement('button');
    this.send.className = 'textchat-compose-send';
    this.send.innerText = 'Send';

    this.compose.appendChild(this.message);
    this.compose.appendChild(this.send);

    this.element.appendChild(this.messages);
    this.element.appendChild(this.compose);

    this.message.addEventListener('keydown', ({ key }) => { if (key === 'Enter') this.sendMessage(); });

    this.send.addEventListener('click', this.sendMessage);

    this.state.connection.onmessage = (event) => {
      this.addMessage(event.userid, event.data);
    };
  }

  sendMessage() {
    if (this.message.value === '') return;
    this.addMessage(null, this.message.value);
    this.state.connection.send(this.message.value);
    this.message.value = '';
  }

  addMessage(user, text) {
    const message = document.createElement('div');
    const userEl = document.createElement('span');
    const textEl = document.createElement('span');
    message.className = 'textchat-messages-message';
    userEl.className = 'textchat-messages-message-user';
    textEl.className = 'textchat-messages-message-text';

    message.appendChild(userEl);
    message.appendChild(textEl);
    userEl.innerHTML = user || 'you';
    textEl.innerHTML = text;
    this.messages.appendChild(message);
    this.messages.scrollTo(0, this.messages.scrollHeight);
  }
}
