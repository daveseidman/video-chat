export default class Overlays {
  constructor(state) {
    this.state = state;
    this.element = document.createElement('div');
    this.element.className = 'overlays hidden';

    this.window = document.createElement('div');
    this.window.className = 'overlays-window';

    this.content = document.createElement('img');
    this.content.className = 'overlays-window-content';

    this.close = document.createElement('button');
    this.close.innerText = '×';
    this.close.className = 'overlays-close';

    this.close.addEventListener('click', () => { this.state.overlay = null; });
    this.window.appendChild(this.content);

    this.element.appendChild(this.window);
    this.element.appendChild(this.close);
  }

  show(asset) {
    this.content.src = `assets/images/${asset}.png`;
    this.window.scrollTo(0, 0);
    this.element.classList.remove('hidden');
  }

  hide() {
    this.element.classList.add('hidden');
  }
}
