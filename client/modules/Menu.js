export default class Menu {
  constructor(state) {
    this.state = state;
    this.element = document.createElement('div');
    this.element.className = 'menu';

    this.connect = document.createElement('button');
    this.docs = document.createElement('button');
    this.survey = document.createElement('button');
    this.isiButton = document.createElement('button');
    this.piButton = document.createElement('button');
    this.chat = document.createElement('button');

    this.connect.classList = 'menu-connect';
    this.docs.classList = 'menu-docs';
    this.survey.classList = 'menu-survey';
    this.isiButton.classList = 'menu-isi';
    this.piButton.classList = 'menu-pi';
    this.chat.classList = 'menu-chat';

    this.left = document.createElement('div');
    this.left.className = 'menu-left';

    this.right = document.createElement('div');
    this.right.className = 'menu-right';

    this.left.appendChild(this.connect);
    this.left.appendChild(this.docs);
    this.left.appendChild(this.survey);
    this.right.appendChild(this.isiButton);
    this.right.appendChild(this.piButton);
    this.right.appendChild(this.chat);

    this.element.appendChild(this.left);
    this.element.appendChild(this.right);

    this.connect.addEventListener('click', () => { this.state.connect = !this.state.connect; this.connect.classList[this.connect.classList.contains('active') ? 'remove' : 'add']('active'); });
    this.docs.addEventListener('click', () => { this.state.docs = true; });
    this.survey.addEventListener('click', () => { this.state.survey = true; });
    this.isiButton.addEventListener('click', () => { this.state.overlay = 'isi'; });
    this.piButton.addEventListener('click', () => { this.state.overlay = 'pi'; });
    this.chat.addEventListener('click', () => { this.state.chat = !this.state.chat; });
  }

  removeConnectIcon() {
    this.left.removeChild(this.connect);
  }
}
