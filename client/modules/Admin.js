// after logging in, room codes can be generated and shared

import { copyToClipboard } from './Utils';

export default class Admin {
  constructor(state) {
    this.state = state;

    this.login = this.login.bind(this);
    this.showCode = this.showCode.bind(this);

    this.url = window.location.href.indexOf('localhost') >= 0 ? 'https://localhost:8080/' : 'https://rx-co.de/360boothplus/';

    this.addGuest = this.addGuest.bind(this);

    this.element = document.createElement('div');
    this.element.className = 'admin';

    this.loginContainer = document.createElement('div');
    this.loginContainer.className = 'admin-login';

    this.username = document.createElement('input');
    this.username.className = 'admin-login-username';
    this.username.type = 'text';
    this.username.placeholder = 'USERNAME';

    this.password = document.createElement('input');
    this.password.className = 'admin-login-password';
    this.password.type = 'password';
    this.password.placeholder = 'PASSWORD';

    this.submit = document.createElement('button');
    this.submit.className = 'admin-login-submit';
    this.submit.innerText = 'Submit';

    this.username.addEventListener('focus', () => { this.username.classList.remove('error'); });
    this.password.addEventListener('focus', () => { this.password.classList.remove('error'); });
    this.submit.addEventListener('click', this.login);

    this.loginContainer.appendChild(this.username);
    this.loginContainer.appendChild(this.password);
    this.loginContainer.appendChild(this.submit);

    this.tour = document.createElement('div');
    this.tour.className = 'admin-tour';

    this.hostTour = document.createElement('button');
    this.hostTour.className = 'admin-tour-button';
    this.hostTour.innerText = 'Host a Tour';
    this.hostTour.addEventListener('click', () => {
      this.state.host = true;
    });

    this.guestName = document.createElement('input');
    this.guestName.type = 'text';
    this.guestName.placeholder = 'Name (optional)';
    this.guestName.className = 'admin-tour-name';

    this.tourLink = document.createElement('p');
    this.tourLink.innerText = this.url;
    this.tourLink.className = 'admin-tour-link disabled';
    this.tourLink.addEventListener('click', () => {
      copyToClipboard(this.tourLink.innerText);
      this.tourLink.classList.add('copied');
      setTimeout(() => { this.tourLink.classList.remove('copied'); }, 3000);
    });

    this.status = document.createElement('p');
    this.status.className = 'admin-tour-status';

    this.tour.appendChild(this.hostTour);
    this.tour.appendChild(this.guestName);
    this.tour.appendChild(this.tourLink);
    this.tour.appendChild(this.status);


    this.element.appendChild(this.loginContainer);
    // this.authenticated(); // TODO: remove this
  }

  addGuest(event) {
    this.status.innerText = `guest arrived: ${event.userid}`;
  }


  authenticated() {
    this.element.appendChild(this.tour);
    this.loginContainer.classList.add('hidden');
  }

  showCode(roomid) {
    this.tourLink.innerHTML = `${this.url}?roomid=${roomid}${this.guestName.value !== '' ? `&guest=${this.guestName.value}` : ''}`;
    this.tourLink.classList.remove('disabled');
    // this.setup.classList.remove('disabled');
    this.status.innerText = 'waiting for guests...';
  }

  login() {
    if (this.username.value === '') {
      this.username.classList.add('error');
    }
    if (this.password.value === '') {
      this.password.classList.add('error');
    }

    if (this.username.value !== '' && this.password.value !== '') {
      this.loginContainer.classList.add('disabled');

      fetch(`login/${this.username.value}/${this.password.value}`).then(res => res.json()).then(({ success, message }) => {
        this.loginContainer.classList.remove('disabled');
        if (success) {
          this.authenticated();
          this.state.connection.userid = this.username.value;
        } else {
          console.log(message);
          if (message.indexOf('username') >= 0) this.username.classList.add('error');
          if (message.indexOf('password') >= 0) this.password.classList.add('error');
        }
      });
    }
  }
}
