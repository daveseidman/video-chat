export default class Connect {
  constructor(state) {
    this.state = state;
    this.checkCode = this.checkCode.bind(this);

    this.element = document.createElement('div');
    this.element.className = 'connect hidden';

    this.title = document.createElement('h1');
    this.title.innerText = 'Enter a room ID to join your host';
    this.title.className = 'connect-title';

    this.form = document.createElement('div');
    this.form.className = 'connect-form';

    this.code = document.createElement('input');
    this.code.type = 'text';
    this.code.className = 'connect-form-code';
    this.code.placeholder = 'room id';
    this.code.setAttribute('maxlength', 4);
    this.code.addEventListener('input', this.checkCode);

    this.name = document.createElement('input');
    this.name.type = 'text';
    this.name.className = 'connect-form-name';
    this.name.placeholder = 'your name (optional)';

    this.submit = document.createElement('button');
    this.submit.innerText = 'Connect';
    this.submit.className = 'connect-form-submit disabled';
    this.submit.addEventListener('click', () => { this.state.roomID = this.code.value; });

    this.close = document.createElement('button');
    this.close.innerText = '×';
    this.close.className = 'connect-close';
    this.close.addEventListener('click', () => { this.state.connect = false; });

    this.form.appendChild(this.code);
    this.form.appendChild(this.name);
    this.form.appendChild(this.submit);

    this.element.appendChild(this.title);
    this.element.appendChild(this.form);
    this.element.appendChild(this.close);
  }

  show() {
    this.element.classList.remove('hidden');
  }

  hide() {
    this.element.classList.add('hidden');
  }

  disable() {
    this.submit.classList.add('disabled');
  }

  enable() {
    this.submit.classList.remove('disabled');
  }

  checkCode() {
    this.code.classList.remove('error');
    this.submit.classList[this.code.value.length === 4 ? 'remove' : 'add']('disabled');
  }

  error(e) {
    console.log('error', e);
  }
}
