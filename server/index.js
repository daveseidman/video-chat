require('dotenv').config();

const express = require('express');
const fs = require('fs');
const http = require('http');
const https = require('https');
const RTCMultiConnectionServer = require('rtcmulticonnection-server');
const { users } = require('../private/users.json');

const app = express();

app.use('/360boothplus', express.static(`${__dirname}/../dist`));

app.get(process.env.LOCAL === 'true' ? '/login/:user/:pass' : '/360boothplus/login/:user/:pass', (req, res) => {
  const { user, pass } = req.params;
  const matchedUser = users.filter(item => item.username === user);
  if (matchedUser.length > 0) {
    if (matchedUser[0].password === pass) {
      res.send({ success: true });
    } else {
      res.send({ success: false, message: 'incorrect password' });
    }
  } else {
    res.send({ success: false, message: 'username not found ' });
  }
});

const server = process.env.SSL === 'true'
  ? http.createServer(app)
  : https.createServer({ key: fs.readFileSync('private/privatekey.pem'), cert: fs.readFileSync('private/certificate.pem') }, app);

RTCMultiConnectionServer.beforeHttpListen(server, {});
server.listen(process.env.PORT, '0.0.0.0', () => {
  RTCMultiConnectionServer.afterHttpListen(server, {});
});

const io = require('socket.io').listen(server); // eslint-disable-line

io.on('connection', (socket) => {
  RTCMultiConnectionServer.addSocket(socket, {});
});
